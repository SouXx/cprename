/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   nameCutter.cpp
 * Author: tobi
 * 
 * Created on August 1, 2018, 2:07 PM
 */

#include <string>

#include "nameCutter.h"

nameCutter::nameCutter(std::string filename) {

    if (filename.size() > 32) {
        this -> filename = filename;
        this -> length = filename.length();
    }
}

nameCutter::~nameCutter() {
}

std::string nameCutter::cutDXF() {
    this->cut32();
    int eIdx = filename.find(".pdf");
    filename.insert(eIdx, "_DXF");
    return filename;
}

std::string nameCutter::cut32() {
    int eIdx = filename.find(".pdf");
    eIdx = eIdx - 32;
    filename.erase(32, eIdx);
    return filename;
}


