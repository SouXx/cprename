/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "catch.hpp"
#include <iostream>

#include "nameCutter.h"

TEST_CASE("000-nameCutter") {


    using Catch::Matchers::EndsWith;
    std::string longerThan32DXF("d53074043001w7004101010103d01a00.CATDrawing_DXF.pdf");

    SECTION("cut32") {
        nameCutter cutter(longerThan32DXF);
        std::string str = cutter.cut32();
        REQUIRE_THAT(str, EndsWith(".pdf"));
    }

    SECTION("cutDXF") {
        nameCutter cutter(longerThan32DXF);
        std::string str = cutter.cutDXF();
        REQUIRE_THAT(str, EndsWith(".pdf") && Catch::Matchers::Contains("_DXF"));
    }

}