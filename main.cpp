/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: tobi
 *
 * Created on July 27, 2018, 10:51 AM
 */
#define CATCH_CONFIG_RUNNER
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "catch.hpp"
#include <experimental/filesystem>

#include "nameCutter.h"

/*
 * 
 */
int main(int argc, char** argv) {

    Catch::Session session;
    namespace ca = Catch::clara;
    auto cli = session.cli();
    auto exitCode = session.applyCommandLine(argc, argv);
    session.run();

    namespace fs = std::experimental::filesystem::v1;
    fs::path copyPath;
    fs::path pastePath;
    fs::path tmpCp;
    fs::path tmpPst;
    std::string newName;
    std::string pathFrom, str;
    int renamedFiles = 0;
    int foundFiles = 0;



    std::cout << "Path from: " << std::endl;
    std::cin >> pathFrom;

    copyPath = fs::path(pathFrom);
    pastePath = fs::path(pathFrom);
    tmpCp /= copyPath;
    pastePath /= "renamedFiles";

    if (!fs::exists(pastePath))
        fs::create_directory(pastePath);

    //logfile
    fs::permissions(pastePath, fs::perms::others_all | fs::perms::all);
    fs::path logPath = pastePath / "cpRenameLog.csv";
    std::ofstream logFile;
    logFile.open("cpRenameLog.csv");
    logFile << "Copy from Path: " << "," << copyPath << std::endl;
    logFile << "to: " << "," << pastePath << std::endl;
    logFile << "Old name" << "," << "New name" << std::endl;



    for (auto& p : fs::directory_iterator(copyPath)) {

        str = p.path().filename().string();
        logFile << str << ",";
        tmpPst = pastePath;

        if (!str.empty() && str.find(".pdf") != std::string::npos && str.find("JTT") == std::string::npos) {
            nameCutter cutter(str);
            tmpCp = tmpCp/str;
            if (str.find("_DXF") != std::string::npos)
                newName = cutter.cutDXF();
            else
                newName = cutter.cut32();

            try {
                fs::rename(tmpCp, copyPath / str);
                fs::rename(tmpCp, pastePath / newName);
                renamedFiles++;
                logFile << newName << std::endl;
            } catch (const fs::filesystem_error& e) {
                std::cerr << "Error: " << e.what() << std::endl;
            }
        } else 
            logFile << " - " << std::endl;
        tmpCp = copyPath;
        foundFiles++;
    }

    logFile << "found files: " << "," << foundFiles << std::endl;
    logFile << "renamed files: " << "," << renamedFiles << std::endl;
    logFile.close();
    return 0;
}



