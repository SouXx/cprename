/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   nameCutter.h
 * Author: tobi
 *
 * Created on August 1, 2018, 2:07 PM
 */

#ifndef NAMECUTTER_H
#define NAMECUTTER_H
#include <string>
#include <iostream>

class nameCutter {
public:

    nameCutter(std::string filename);
    virtual ~nameCutter();

    std::string cut32();
    std::string cutDXF();
private:
    std::string filename;
    int length;
};

#endif /* NAMECUTTER_H */

